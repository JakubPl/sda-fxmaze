package pl.sda;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    @FXML
    private Canvas canvas;

    private int heroX = 0, heroY = 40;

    private String[][] maze;
    private int mazeHeight;
    private int mazeWidth;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.setFill(Color.BLUE);
        gc.fillRect(heroX, heroY, 10, 10);
        canvas.setFocusTraversable(true);
        canvas.setOnKeyPressed(e -> {
            solveMaze(gc, mazeHeight, mazeWidth);
        });


        List<String> lines;
        try {
            lines = Files.readAllLines(Paths.get("maze.txt"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        mazeHeight = lines.size();
        mazeWidth = lines.get(0).length();

        maze = new String[mazeHeight][mazeWidth];

        int yToTab = 0;
        for (String line : lines) {
            maze[yToTab] = line.split("");
            yToTab++;
        }

        gc.setFill(Color.RED);
        for (int y = 0; y < mazeHeight; y++) {
            for (int x = 0; x < mazeWidth; x++) {
                if (maze[y][x].equals("*")) {
                    gc.fillRect(x * 10, y * 10, 10, 10);
                }
            }
        }

    }

    private void solveMaze(GraphicsContext gc, int mazeHeight, int mazeWidth) {
        while (!maze[heroY / 10][heroX / 10].equals("X")) {
            int currentHeroYMazePosition = heroY / 10;
            int currentHeroXMazePosition = heroX / 10;

            if (currentHeroYMazePosition + 1 < mazeWidth && !maze[currentHeroYMazePosition + 1][currentHeroXMazePosition].equals("*")) {
                currentHeroYMazePosition += 1;
            } else if (currentHeroYMazePosition - 1 >= 0 && !maze[currentHeroYMazePosition - 1][currentHeroXMazePosition].equals("*")) {
                currentHeroYMazePosition -= 1;
            } else if (currentHeroXMazePosition + 1 < mazeHeight && !maze[currentHeroYMazePosition][currentHeroXMazePosition + 1].equals("*")) {
                currentHeroXMazePosition += 1;
            } else if (currentHeroXMazePosition - 1 >= 0 && !maze[currentHeroYMazePosition][currentHeroXMazePosition - 1].equals("*")) {
                currentHeroXMazePosition -= 1;
            }
            gc.setFill(Color.BLUE);
            gc.fillRect(currentHeroXMazePosition * 10,currentHeroYMazePosition * 10, 10, 10);
            heroY = currentHeroYMazePosition * 10;
            heroX = currentHeroXMazePosition * 10;
        }
    }
}
